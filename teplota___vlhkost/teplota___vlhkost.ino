/**
   BasicHTTPClient.ino

    Created on: 24.05.2015

*/
#include "DHT.h"
#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#define DHTPIN 2
#define DHTTYPE DHT11

ESP8266WiFiMulti WiFiMulti;
DHT dht(DHTPIN, DHTTYPE);

void setup() {
 
  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  Serial.println();
  Serial.println();
  Serial.println();
  dht.begin();
  
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("D31-lab", "IoT.SPSE.lab22");
}

int i=1;

void loop() {
  float t = dht.readTemperature();
  float h = dht.readHumidity();
  i++;
  if(i>25) i= 0;
  
  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {

    WiFiClient client;
//zde
    HTTPClient http;


    Serial.print("[HTTP] begin...\n");
    if (http.begin(client, "http://thingsboard.cloud/api/v1/g29NfU3MZpbppBBFDsjw/telemetry")) {  // HTTP   //přidání wifi z nového kodu

      String string = "";
      string += "{temperature:"; //teplota
      string += String(t);
      string += ",humidity:";  //vlhkost (oddělit ",")
      string += String(h);
      string +="}";




      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.POST(string);

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

     
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.println("[HTTP] Unable to connect");
    }
  }

  delay(250);
}
